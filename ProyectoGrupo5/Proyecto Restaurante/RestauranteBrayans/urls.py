"""RestauranteBrayans URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from RestauranteApi import views

urlpatterns = [
    
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('mesa/', views.MesaCreateView.as_view()),
    path('mesalist/', views.MesaListView.as_view()),
    path('mesa/<int:pk>/', views.MesaDetailView.as_view()),

    path('reserva/', views.ReservaCreateView.as_view()),
    path('reserva/<int:pk>/', views.ReservaDetailView.as_view()),
    path('reservalist/', views.ReservaListView.as_view())
    #path('cliente/', views.ClienteCreateView.as_view({'get': 'list'})),
]
