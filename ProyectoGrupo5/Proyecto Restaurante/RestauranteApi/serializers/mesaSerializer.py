from RestauranteApi.models.mesa import Mesa
from rest_framework import serializers

class MesaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mesa
        fields = ['idmesa', 'numeroMesa', 'descricionMesa']
        
    def create(self, validated_data):
        mesaInstance = Mesa.objects.create(**validated_data)
        return mesaInstance
    
    def to_representation(self, obj):
        mesa = Mesa.objects.get(idmesa = obj.idmesa)
        return {'idmesa': mesa.idmesa,
                'numeroMesa': mesa.numeroMesa,
                'descricionMesa': mesa.descricionMesa
                }