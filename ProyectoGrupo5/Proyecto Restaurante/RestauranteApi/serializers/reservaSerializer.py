from RestauranteApi.models.mesa import Mesa
from RestauranteApi.models.reseva import Reserva
from rest_framework import fields, serializers
from RestauranteApi.serializers.mesaSerializer import MesaSerializer

class ReservaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reserva
        fields = ['idreserva', 'nombreCl','numeroMesa','horaInicio','fechaReserva']       
    def create(self, validated_data):
        reservaInstance = Reserva.objects.create(**validated_data)
        return reservaInstance

    
    def to_representation(self, obj):
        reserva = Reserva.objects.get(idreserva = obj.idreserva)
        
        return {    'idreserva': reserva.idreserva,
                    'nombreCl': reserva.nombreCl ,
                    'numeroMesa': reserva.numeroMesa,                    
                    'horaInicio': reserva.horaInicio ,
                    'fechareserva': reserva.fechaReserva,
                    
                }