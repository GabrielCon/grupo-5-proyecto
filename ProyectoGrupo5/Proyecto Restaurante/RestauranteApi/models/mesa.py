from django.db import models

class Mesa (models.Model):
    idmesa = models.BigAutoField(primary_key= True)
    numeroMesa = models.IntegerField("NumeroMesa")
    descricionMesa = models.CharField('DescripcionMesa',max_length=1000)


