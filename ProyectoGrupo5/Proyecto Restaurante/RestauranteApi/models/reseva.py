from django.db import models
from .user import User
from .mesa import Mesa


class Reserva(models.Model):
    idreserva = models.BigAutoField(primary_key=True)
    nombreCl = models.CharField("NombreCliente", max_length= 255)
    numeroMesa = models.IntegerField("NumeroMesa")
    horaInicio = models.TimeField('HoraInicio',auto_now_add=False, auto_now=False)
    fechaReserva = models.DateField('FechaReserva',auto_now=False, auto_now_add=False)
