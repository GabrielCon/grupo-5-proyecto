from django.apps import AppConfig


class RestauranteapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'RestauranteApi'
