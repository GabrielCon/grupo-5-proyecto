from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, serializers, status
from rest_framework import views , viewsets
from rest_framework import response
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins
from rest_framework import generics
from RestauranteApi.models.reseva import Reserva
from RestauranteApi.serializers.reservaSerializer import ReservaSerializer

class ReservaCreateView(mixins.CreateModelMixin,
                   generics.GenericAPIView):
    queryset = Reserva.objects.all()
    serializer_class = ReservaSerializer
    
    def post(self,request,*args,**kwargs):
        serializer = ReservaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.validated_data, status= status.HTTP_201_CREATED)
'''
    def get(self, request,*args,**kwargs):
        reserva = Reserva.objects.all()
        serializer = ReservaSerializer
        return Response(serializer.data)'''
