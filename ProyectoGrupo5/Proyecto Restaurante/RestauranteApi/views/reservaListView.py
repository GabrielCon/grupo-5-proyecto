from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, serializers, status
from rest_framework import mixins
from rest_framework import generics
from RestauranteApi.models.reseva import Reserva
from RestauranteApi.serializers.reservaSerializer import ReservaSerializer

class ReservaListView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Reserva.objects.all()
    serializer_class = ReservaSerializer

    def get(self, request,*args,**kwargs):
        return self.list(request,*args,**kwargs)
    
