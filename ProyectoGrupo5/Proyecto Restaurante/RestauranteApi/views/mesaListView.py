from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, serializers, status
from rest_framework import mixins
from rest_framework import generics

from RestauranteApi.models.mesa import Mesa
from RestauranteApi.serializers.mesaSerializer import MesaSerializer

class MesaListView(mixins.ListModelMixin,generics.GenericAPIView):
    queryset = Mesa.objects.all()
    serializer_class = MesaSerializer
    
    def get(self, request,*args,**kwargs):
        return self.list(request,*args,**kwargs)
