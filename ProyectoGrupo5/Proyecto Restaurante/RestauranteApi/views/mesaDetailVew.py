from rest_framework import mixins
from rest_framework import generics
from RestauranteApi.models.mesa import Mesa
from RestauranteApi.serializers.mesaSerializer import MesaSerializer

class MesaDetailView(mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = Mesa.objects.all()
    serializer_class = MesaSerializer

    def get(self, request,*args,**kwargs):
        return self.retrieve(request,*args,**kwargs)
    def put(self, request,*args,**kwargs):
        return self.update(request,*args,**kwargs)
    def delete(self, request,*args,**kwargs):
        return self.destroy(request,*args,**kwargs)