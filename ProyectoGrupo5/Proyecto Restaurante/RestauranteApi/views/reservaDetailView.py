from rest_framework import mixins
from rest_framework import generics
from RestauranteApi.models.reseva import Reserva
from RestauranteApi.serializers.reservaSerializer import ReservaSerializer

class ReservaDetailView(mixins.RetrieveModelMixin,mixins.UpdateModelMixin,mixins.DestroyModelMixin, generics.GenericAPIView):
    queryset = Reserva.objects.all()
    serializer_class = ReservaSerializer

    def get(self, request,*args,**kwargs):
        return self.retrieve(request,*args,**kwargs)
    def put(self, request,*args,**kwargs):
        return self.update(request,*args,**kwargs)
    def delete(self, request,*args,**kwargs):
        return self.destroy(request,*args,**kwargs)