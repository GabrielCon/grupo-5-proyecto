from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, serializers, status
from rest_framework.response import Response
from rest_framework import mixins
from rest_framework import generics

from RestauranteApi.models.mesa import Mesa
from RestauranteApi.serializers.mesaSerializer import MesaSerializer

class MesaCreateView(mixins.CreateModelMixin,generics.GenericAPIView):
    queryset = Mesa.objects.all()
    serializer_class = MesaSerializer
    
    def post(self,request,*args,**kwargs):
        serializer = MesaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.validated_data, status= status.HTTP_201_CREATED)


