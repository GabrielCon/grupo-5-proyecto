import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import InicIo from './components/InicIo.vue'
import Home from './components/Home.vue'
import Account from './components/Account.vue'
import ReservA from './components/ReservA.vue'
import MesA from './components/MesA.vue'


const routes = [{
    path: '/',
    name: 'root',
    component: App
  },
  {
    path: '/user/logIn',
    name: "logIn",
    component: LogIn  
  },
  {
    path: '/user/signUp',
    name: "signUp",
    component: SignUp
  },
  {
    path: '/user/inicIo',
    name: "inicIo",
    component: InicIo  
  },
  {
    path: '/user/home',
    name: "home",
    component: Home
  },
  {
    path: '/user/account',
    name: "account",
    component: Account

  },
  {
    path: '/user/reservas',
    name: "reservA",
    component: ReservA

  },
  {
    path: '/user/MesA',
    name: "mesA",
    component: MesA

  },
  

]

const router = createRouter({
  history: createWebHistory(),
  routes
})
export default router